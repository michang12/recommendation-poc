'use strict'
const { handleRequestApi } = require('../../src')

const handleMethodOfCalling = (req, res, next) => {
  console.log(req.path)
  if (req.method === 'OPTIONS') {
      res.set('Access-Control-Allow-Origin', '*');
      // Send response to OPTIONS requests
      res.set('Access-Control-Allow-Methods', 'GET');
      res.set('Access-Control-Allow-Headers', 'Content-Type');
      res.set('Access-Control-Max-Age', '3600');
      res.status(204).send('');
  } else if (req.method !== 'GET') {
      res.status(405).send('');
  }
  return next(req, res)
}

// path of url will be {{domain_serverless}}/restaurants/{{resourceId}}
module.exports = (req, res) => {
  return handleMethodOfCalling(req, res, handleRequestApi)  
};