const { BigQuery } = require('@google-cloud/bigquery');

const bigquery = new BigQuery({
    projectId: process.env.GOOGLE_CLOUD_PROJECT,
    keyFilename: process.env.GOOGLE_APPLICATION_CREDENTIALS
});

const makeQuery = async (query) => {
    console.log(query);
    try {
        // For all options, see https://cloud.google.com/bigquery/docs/reference/rest/v2/jobs/query
        const options = {
            query,
            // Location must match that of the dataset(s) referenced in the query.
            location: 'US',
        };

        // Run the query as a job
        const [job] = await bigquery.createQueryJob(options);

        // Wait for the query to finish
        const [rows] = await job.getQueryResults();
        return {
            success: true,
            data: rows,
            error: null
        }
    } catch (error) {
        console.log(error);
        return {
            success: false,
            data: [],
            error
        }
    }
}

module.exports = {
    makeQuery
}
