module.exports = {
    NOT_SUPPORT_THIS_QUERY: 'This query is not supported',
    AVAILABLE_IN_THE_FUTURE: 'This query is only supported in the future',
    GOT_ERROR_WHEN_PROCESS_FUNCTION: (functionName) => `Fail to process function ${functionName}`
}
