const express = require('express')
const { handleRequestApi, handleRestaurantsRequestApi, handleUsersRequestApi } = require("./src");

const app = express()
const port = 3000

//for local testing
app.get('/restaurants/:resourceId', async (req, res) => {
    const params = {
        resourceId: req.params.resourceId
    }
    return handleRestaurantsRequestApi(params, res)
})

app.get('/:target/:resourceId/:type', async (req, res) => {
    return handleRequestApi(req, res)
})

app.get('/users/:resourceId/:type', async (req, res) => {
    return handleUsersRequestApi(req.params, res)
})

app.get('/users/:resourceId', async (req, res) => {
    return handleUsersRequestApi(req.params, res)
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
