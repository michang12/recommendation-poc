const { getRecommendedRestaurantsById } = require('./restaurants');
const { getRecommendedRestaurantsByUserId } = require('./customers');

const errorMessage = require('../helpers/errorMessage')

const handleRequestApi = async (req, res) => {
    try {
        // req.path have format: /:resource/:resourceId/:type
        const pathVars = req.path.substring(1).split("/");
        const params = {
            resource: pathVars[0],
            resourceId: pathVars[1],
            type: pathVars[2]
        }
        if (params.resourceId) {
            switch (params.resource) {
                case 'restaurants':
                    return handleRestaurantsRequestApi(params, res);
                case 'categories':
                    // TODO: add handle for categories
                    res.status(500).send(errorMessage.AVAILABLE_IN_THE_FUTURE);
                    break;
                 case 'users':
                    // TODO: add handle for users
                     res.status(500).send(errorMessage.AVAILABLE_IN_THE_FUTURE);
                    break;
                default:
                    res.status(500).send(errorMessage.NOT_SUPPORT_THIS_QUERY);
            }
        }
        res.status(500).send(errorMessage.NOT_SUPPORT_THIS_QUERY);
    } catch (error) {
        res.status(500).send(error.toString());
    }
}

const handleRestaurantsRequestApi = (params, res) => {
    if(params.type === undefined) {
        return getRecommendedRestaurantsById(params.resourceId, res)
    } else {
        switch (params.type) {
              case 'item-a':
                  // TODO: add handle for item, eg "popular cuisines"
                  res.status(500).send(errorMessage.AVAILABLE_IN_THE_FUTURE);
                break;
            default:
                res.status(500).send(errorMessage.NOT_SUPPORT_THIS_QUERY);
        }
    }
}


const handleUsersRequestApi = (params, res) => {
    if(params.type === undefined) {
        return getRecommendedRestaurantsByUserId(params.resourceId, res)
    }
}


module.exports = {
    handleRequestApi, handleRestaurantsRequestApi, handleUsersRequestApi
}
