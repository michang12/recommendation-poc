const { makeQuery } = require('../helpers/bigQueryCaller')
const errorMessage = require('../helpers/errorMessage')

const dataResponse = 'rec_menuId as menuId, rec_menuName as menuName, rec_logo as logo, rank, score'

const recommendByMenuSimilarity = (restaurantId) => `SELECT ${dataResponse} from bqml.menu_sim_recommend where menu_id = '${restaurantId}' limit 10;`;
const recommendByCuisineSimilarity = (restaurantId) => `SELECT ${dataResponse} from bqml.menu_sim_by_tags where menu_id = '${restaurantId}' limit 10;`;

const getRecommendedRestaurantsById = async (restaurantId, res) => {
    try {
        const { success, data, error } = await makeQuery(recommendByMenuSimilarity(restaurantId))
        if (success) {
            if (data.length > 0) {
                res.status(200).send(data);
            } else {
                return getRecommendedRestaurantViaFallback(restaurantId, res)
            }
        } else {
            console.log(error)
            res.status(500).send(errorMessage.GOT_ERROR_WHEN_PROCESS_FUNCTION('getRecommendedRestaurantViaFallback'));
        }
    } catch (error) {
        console.log(error)
        res.status(500).send(errorMessage.GOT_ERROR_WHEN_PROCESS_FUNCTION('getRecommendedRestaurantsById'));
    }
}

const getRecommendedRestaurantViaFallback = async (restaurantId, res) => {
    try {
        const { success, data, error } = await makeQuery(recommendByCuisineSimilarity(restaurantId))
        if (success) {
            res.status(200).send(data);
        } else {
            console.log(error)
            res.status(500).send(errorMessage.GOT_ERROR_WHEN_PROCESS_FUNCTION('getRecommendedRestaurantViaFallback'));
        }
    } catch (error) {
        console.log(error)
        res.status(500).send(errorMessage.GOT_ERROR_WHEN_PROCESS_FUNCTION('getRecommendedRestaurantViaFallback'));
    }
}

module.exports = {
    getRecommendedRestaurantsById
}
