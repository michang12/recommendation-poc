const { makeQuery } = require('../helpers/bigQueryCaller')
const errorMessage = require('../helpers/errorMessage')

const recommendByUserProfile = (userId) => `SELECT * FROM ML.RECOMMEND(MODEL bqml.oddleeats_recommender, (SELECT "${userId}" as visitorID)) ORDER BY predicted_rating_confidence DESC LIMIT 5;`;

const dataResponse = 'menuId, name as menuName, logo, cover';
const getRestaurantsByIds = (ids) => `SELECT ${dataResponse} FROM bqml.oddle_eats_airtable where menuId in (${ids});`;

const getRecommendedRestaurantsByUserId = async (userId, res) => {
    try {
        const { success, data, error } = await makeQuery(recommendByUserProfile(userId))
        if (success) {
            // get array of id from array of restaurants
            const restaurantIds = data.map(restaurant => `"${restaurant.itemId}"`)

            populateRestaurantData(restaurantIds).then(function(restaurants){
                res.status(200).send(restaurants);
            });
        } else {
            console.log(error)
            res.status(500).send(errorMessage.GOT_ERROR_WHEN_PROCESS_FUNCTION('getRecommendedRestaurantViaFallback'));
        }
    } catch (error) {
        console.log(error)
        res.status(500).send(errorMessage.GOT_ERROR_WHEN_PROCESS_FUNCTION('getRecommendedRestaurantsById'));
    }
}

const populateRestaurantData = async (ids) => {
    try {
        const { success, data, error } = await makeQuery(getRestaurantsByIds(ids));
         if(success) {
             data.forEach(function(restaurant){
                 restaurant.logo = restaurant.logo != null ? ("https://images.weserv.nl/?url=" + restaurant.logo) : null;
                 restaurant.cover = restaurant.cover != null ? ("https://images.weserv.nl/?url=" + restaurant.cover) : null;
             });
             return data;
        } else {
             console.log(error)
             return [];
        }
    } catch (error) {
        console.log(error)
        res.status(500).send(errorMessage.GOT_ERROR_WHEN_PROCESS_FUNCTION('populateRestaurantData'));
    }
}

module.exports = {
    getRecommendedRestaurantsByUserId
}
