# README

This project is built for Serverless  

## How do I get set up?

### AWS Lambda
Follow step by step at: https://oddle-pm.atlassian.net/wiki/spaces/TD/pages/1963163705/POC+Using+serverless+to+build+API+for+Recommendations+system#Deploy-AWS-Lambda

### Google Cloud Function
* Install Google Cloud cli: https://cloud.google.com/sdk/docs/install
* For deployment: `npm run deploy-gcloud`
* For remove function: `npm run remove-function-gcloud`
* For list all our functions: `npm run list-function-gcloud`

## How do I run on local?
* run `npm install`
* run `npm run dev`
